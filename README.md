# TMT任务管理框架



一个采用面向对象思想封装的适用于裸机的前后台系统框架，本质是时间片运行机制，类似软件定时器。支持市面上所有常见的MCU，移植非常简单。


[点我观看介绍视频](https://www.bilibili.com/video/bv1Ww411d7oD)


> 开源项目QQ交流群：`1001220381`，欢迎加群找我摸鱼~

如果你觉得本项目对你有所帮助，请给我一个Star！



## 详细描述

TMT任务管理框架的本质是时间片任务管理方法。该框架优化了这种分配时间任务的方式，更加灵活易用。



裸机开发中，我们常用的一种程序框架是前后台系统，即中断服务函数为前台，main函数内的无限循环体为后台。变量在前台计数，到达设定的计数值（即一定时间）后，置出相应的标志位，然后在后台判断，并执行相应的代码。

<img src="doc/image/TMT_1.png" style="zoom:75%;" />





传统的前后台系统，虽然是一个比较合理的框架，但是有一些缺点，非常令人头疼。比如变量的传递，尤其设置标志位，一般都是全局变量，且跨文件调用，当分配的全局变量过多时，会降低代码的阅读性和维护性，大大影响我们的开发效率。

本框架在前后台系统的基础上，进行了优化，通过结构体封装+中断回调函数的方式（面向对象的封装思想），基本上避免了以上所述的问题，使我们整个的程序框架更加稳定简洁。

要说明的是，本模块根据时间片来管理任务的，无优先级。任务的执行顺序与分配的时间有关，时间越小，越先执行。如果两个任务被分配的时间相同，先分配的任务，先执行。

<img src="doc/image/TMT_2.png" style="zoom:75%;" />

### 约束

* TMT调度的时间，受两个方面影响：一个是本身设置的调度时间（单位ticks），一个是所有任务运行的总时间。
* 一个任务只有等前面的任务运行完毕，才会得到运行。
* 在任务中调用延时函数，会阻塞所有任务，直到退出延时。



## API手册

只有七个API函数，可以根据需要，搭配使用，非常简单。

|                           函数名                           |     描述     |
| :--------------------------------------------------------: | :----------: |
|                       **TMT.run**()                        |   TMT运行    |
|                       **TMT.Tick**()                       |   TMT节拍    |
|  **TMT.Create**(void (*taskFunc) (void),uint16_t triTime)  |   创建任务   |
|          **TMT.Delete**(void (*taskFunc) (void))           |   删除任务   |
| **TMT.TimeCtrl**(void (*taskFunc) (void),uint16_t triTime) | 任务时间控制 |
|  **TMT.runCtrl**(void (*taskFunc) (void),PRO_STATE state)  | 任务状态控制 |
|                    void **TMT_Init**()                     |  TMT初始化   |



## 代码演示

1. 在TMT.h文件里，配置相关参数 ；

```C
/**
 * @brief     硬件平台相关的头文件，提供硬件运行的基本环境，一般是寄存器头文件。
 * @details   The header files related to the hardware platform provide 
 *            the basic environment for hardware operation, generally 
 *            register header files.
**/
#include 	  "STC8Ax_REG.h" 


/**
 * @brief     进入TMT临界区宏函数，需要关闭相关定时器的中断。
 * @details   Enter the TMT critical section macro function.
 *            It is necessary to turn off the interrupt of the related timer.
 * @note      需要根据硬件平台，移植合适的关定时器中断代码。
 *            It is necessary to transplant the appropriate off-timer 
 *            interrupt code according to the hardware platform.
**/
#define       ENTER_TMT_CRI_AREA()    do{ ET0 = 0; }while(0)


/**
 * @brief     退出TMT临界区宏函数，需要打开相关定时器的中断。
 * @details   Enter the TMT critical section macro function.
 *            It is necessary to turn on the interrupt of the related timer.
 * @note      需要根据硬件平台，移植合适的开定时器中断代码。
 *            It is necessary to transplant the appropriate on-timer 
 *            interrupt code according to the hardware platform.
**/
#define       EXTI_TMT_CRI_AREA()     do{ ET0 = 1; }while(0)


/**
 * @brief     TMT的变量类型重定义，如果有冲突，请配置为0。
 * @details   The variable type of TMT is redefined. 
 *            If there is a conflict, please configure it to 0..
**/
#if 		  (0)

	typedef  unsigned char   uint8_t; /*!<  8 bits */
	typedef  unsigned int   uint16_t; /*!< 16 bits */

#endif


/**
 * @brief     任务数量控制宏，用来确定TMT需要调度的最大任务数量。
 * @details   The number of tasks control macro is used to 
 *            determine the maximum number of tasks that TMT
 *            needs to schedule.
**/
#define 	  TMT_TASK_NUM  	 (8)


```



2. 在定时器中断服务函数里，调用任务时间处理回调函数 ；

```C
#include "TMT.h"  //调用头文件；

void TIMER0_ISRQ_Handler(void) //定时器0中断服务函数
{
    /*
    调用TMT节拍函数，为TMT提供心跳节拍，
    节拍时间根据你设置的定时器中断时间来决定
    */
    TMT.Tick(); 
}
```

3. 在自己编写的C文件里，编写若干个任务。

```C
void App_lamp1(void) //任务必须是void型 无形参
{
    GPIO_TOGGLE_PIN(GPIO_P1,Pin_0);
}

void App_lam2p(void) //任务必须是void型 无形参
{
    GPIO_TOGGLE_PIN(GPIO_P1,Pin_1);
}
```

4. 在main里面调用进程初始化，同时在循环体里面调用TMT运行函数。

```C

int main(void)  //main 函数
{
    extern void App_lamp1(void);
    extern void App_lamp2(void);

    STC8x_System_Init(); // 调用MCU外设初始化函数  主要初始化定时器，这里给定时器1ms中断一次 
    
    TMT_Init();  //TMT初始化函数。

    TMT.Create(App_lamp1,500); /* 创建一个任务，500tick执行一次 */
    TMT.Create(App_lamp2,500); /* 创建一个任务，500tick执行一次 */
    
    for(;;)  //无限循环体
    {
        TMT.Run();// 调用任务进程处理回调函数 
    }
}
```



运行仿真结果如下：

<img src="doc/image/TMT_3.png" style="zoom:75%;" />



## 参与贡献

1.  星标本仓库
2.  Fork 本仓库
3.  新建 Feat_xxx 分支
4.  提交代码
5.  新建 Pull Request

